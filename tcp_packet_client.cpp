// For more info see: https://beej.us/guide/bgnet/html/
// Highly reccommended guide

#include "tcp_packet_client.h"
#include <cstring>
#include <string>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>


int TcpPacketClient::Connect(char* ip, int port) {
	if (sock_fd != -1) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP Packet Client]\x1b[0m "
                        "Error connecting socket: Socket was already connected\n");
	}

	int rv;
	// sockaddr which can hold either IPv4 or IPv6. Will be cast later
	struct sockaddr_storage serverAddr;
	// hints struct sets sockaddr_storage struct in getaddrinfo()
	struct addrinfo hints;
	// Used to traverse the linked list of sockaddr_storage objects
	struct addrinfo *serverInfo, *p;

	memset(&serverAddr, 0, sizeof(struct sockaddr_storage));
	memset(&hints, 0, sizeof(struct addrinfo));

	hints.ai_family = AF_UNSPEC; 		// Allows either IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM;	// Creates a TCP connection

	// Populates the sockaddr_storage based on the hints we supplied. Returns linked list pointed to be serverInfo
	if ((rv = getaddrinfo(ip, intToCString(port), &hints, &serverInfo)) != 0) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP Packet Client]\x1b[0m Failed to getaddrinfo\n");
		return 1;
	}

	// Loop through the linked list of sock_addrstorage until we can connect
	for (p = serverInfo; p != NULL; p = p->ai_next) {
		if ((sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			continue;
		}
		if (connect(sock_fd, p->ai_addr, p->ai_addrlen) == -1) {
			fprintf(stderr, "%s: \033[0m%s\n", "\033[0;31m[Network Core :: TCP Packet Client]", strerror(errno));
			close(sock_fd);
			continue;
		}
		break;
	}
	// Traversed the whole linked list without connection to any socket
	if (p == NULL) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP Packet Client]\x1b[0m Server failed to bind\n");
		return 1;
	}

	// String to hold the ip. Max size if size of IPv6 ip
	char ipString[INET6_ADDRSTRLEN];
	inet_ntop(p->ai_family, getInAddress((struct sockaddr*)p->ai_addr), ipString, sizeof(ipString));
    fprintf(stderr, "\x1b[32m[Network Core :: TCP Packet Client]\x1b[0m Connected {ip: %s, port: %d}\n", ipString, port);
	freeaddrinfo(serverInfo);

	//Delete after testing
	int numbytes;
	char buf[MaxPacketSize];
    if ((numbytes = recv(sock_fd, buf, MaxPacketSize-1, 0)) == -1) {
        perror("recv");
        exit(1);
    }
    // Terminating c-string with a null terminator
    buf[numbytes] = '\0';
    printf("Client Recieved %s\n", buf);
    close(sock_fd);
	// Delete after testing
	
	return 0;
}

// Checks the family type (IPv4 or IPv6) and returns the ip address after being casted to right struct
void* TcpPacketClient::getInAddress(struct sockaddr* sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	else {
		return &(((struct sockaddr_in6*)sa)->sin6_addr);
	}
}

const char* TcpPacketClient::intToCString(int i) {
	return std::to_string(i).c_str();
}

int main(int argc, char* argv[]) {
	TcpPacketClient client;
	int port = 3490;
	client.Connect(argv[1], port);
}