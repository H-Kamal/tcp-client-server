#include <tcp_packet_server.h>

#include <iostream>
#include <cstring>
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <netdb.h>

int TcpPacketServer::Connect(char* ip, int port) {
	if (sock_fd != -1) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP_Server]\x1b[0m "
                        "Error connecting socket: Socket was already connected\n");
	}

	int rv;
	// Can hold IPv4 or IPv6
	struct sockaddr_storage theirAddr;
	// hints struct sets the sockaddr_storage struct in getaddrinfo
	struct addrinfo hints;
	// serverinfo is the linked list of addrinfo's returned from getaddrinfo
	struct addrinfo *serverinfo, *p;

	memset(&theirAddr, 0, sizeof(struct sockaddr_storage));
	memset(&hints, 0, sizeof(struct addrinfo));

	hints.ai_family = AF_UNSPEC;	 // Allows IP protocol be either IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // Creates a TCP connection
	hints.ai_flags = AI_PASSIVE;	 // Bind to the host machine's IP

	// Populate the sockaddr_storage according to the hints we supplied
	if ((rv = getaddrinfo(NULL, PORT, &hints, &serverinfo)) != 0) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP_Server]\x1b[0m ", gai_strerror(rv), "\n");
		return 1;
	}
	
	// Loop through the linked list of sock_addrstorage until we can bind
	for (p = serverinfo; p != NULL; p = p->ai_next) {
		if ((sock_fd = socket(p->ai_family,p->ai_socktype,
				p->ai_protocol)) == -1) {
			continue;
		}

		// Set the socket to not hog the port number
		// and allow other processes to bind to it if it's not already connected
		int optRetVal = 1;
		if (setsockopt(sock_fd, SO_REUSEADDR, &optRetVal, sizeof(int)) == -1) {
			return 1;
		}

		if (bind(sock_fd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock_fd);
			continue;
		}

		break;
	}
	freeaddrinfo(serverinfo);

	// Made it to the end of the linked list without binding
	if (p == NULL) {
		fprintf(stderr, "\x1b[31m[Network Core :: TCP_Server]\x1b[0m Server failed to bind\n");
	}

	if (listen(sock_fd, BACKLOG) == -1) {
		return 1;
	}

	while(1) {
		// ... Need to do the accept() and read/write
	}

	return 0;

}

// int TcpPacketServer::Disconnect() {

// }

// int TcpPacketServer::sendPacket(void* buffer, int size) {

// }