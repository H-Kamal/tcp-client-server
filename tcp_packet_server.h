#ifndef TCP_PACKET_SERVER_H
#define TCP_PACKET_SERVER_H

#include <iostream>
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 

// TODO: Need to create Windows version
// TODO: Need to include the network_core and inherit and override the methods

#define PORT "3490" 	// The port users will be connecting to
#define BACKLOG 10 		// The number of connections that can queue to connect to server

class TcpPacketServer {
public:
	static const size_t MaxPacketSize = 4096;

	int Connect(char* group, int port);
	int Disconnect();
	int sendPacket(void* buffer, int size);

protected:
	// sock_fd to listen and new_fd to receive new connection
	int sock_fd, new_fd = -1;
};

#endif /* TCP_PACKET_SERVER */