#ifndef TCP_PACKET_CLIENT_H
#define TCP_PACKET_CLIENT_H

// TODO: Need to make windows version
// TODO: Need to inherit networkCore and write the overrides

#include <unistd.h> 

class TcpPacketClient {

public: 
	static const int MaxPacketSize = 4096;
	int Connect(char* ip, int port);
	int Disconnect();
	int sendPacket(void* buffer, int size);

protected:
	int sock_fd = -1;

private:
	void* getInAddress(struct sockaddr *sa);
	const char* intToCString(int i);
};

#endif /* TCP_PACKET_CLIENT_H */